package lafore.ch3.exercises;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by ig on 8/4/2017.
 */
public class Exercise_3_3Test {
    @Test
    public void removeDuplicates() throws Exception {
        List<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(1);
        actual.add(1);
        actual.add(2);

        actual = Exercise_3_3.removeDuplicates(actual);

        assertThat(actual.size(), is(2));

    }

}