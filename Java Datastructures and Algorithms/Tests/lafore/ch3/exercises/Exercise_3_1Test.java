package lafore.ch3.exercises;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;

/**
 * Created by ig on 8/4/2017.
 */
public class Exercise_3_1Test {
    @Test
    public void bidirectionalBubbleSort() throws Exception {
        List<Integer> actual = new ArrayList<>();
        actual.add(50);
        actual.add(3);
        actual.add(2);
        actual.add(10);
        actual.add(1);
        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(10);
        expected.add(50);
        Exercise_3_1.bidirectionalBubbleSort(actual);
        assertThat(actual, contains(expected.toArray()));

    }

}