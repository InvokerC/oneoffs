package lafore.ch3.exercises;

import java.util.List;

/**
 * Created by ig on 8/4/2017.
 */
//The idea is to send the lowest value to the left at each iteration, after it finishes depositing the largest to the right
public class Exercise_3_1 {

    static void bidirectionalBubbleSort(List<Integer> arr) {
        int leftMin = 0;
        int out;
        int in;
        int leftOut;
        //TODO The outer loop can be a while that goes only halfway.
        for (out = arr.size() - 1; out > 1; out--) {
            for (in = 0; in < out; in++) {
                if (arr.get(in) > arr.get(out)) {
                    int temp = arr.get(out);
                    arr.set(out, arr.get(in));
                    arr.set(in, temp);
                }

            }
            for (leftOut = in - 1; leftOut > leftMin; leftOut--) {
                if (arr.get(leftOut) < arr.get(leftOut - 1)) {
                    int temp = arr.get(leftOut - 1);
                    arr.set(leftOut - 1, arr.get(leftOut));
                    arr.set(leftOut, temp);
                }
            }
            leftMin++;
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello world");
    }
}
