package lafore.ch3.exercises;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ig on 8/4/2017.
 */
//Remove duplicates from a sorted array
public class Exercise_3_3 {

    public static List<Integer> removeDuplicates(List<Integer> list) {
        List<Integer> newList = new ArrayList<>();
        newList.add(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            if (!newList.get(newList.size() - 1).equals(list.get(i))){
                newList.add(list.get(i));
            }
        }
        return newList;
    }

    public static void main(String[] args) {

    }
}
